package ictgradschool.industry.lab09.ex02;

import sun.security.provider.SHA;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

/**
 * Created by Andrew Meads on 25/03/2017.
 */
public class ShapeBox {

    private List<Shape> storage;

    public ShapeBox() {

        this.storage = new LinkedList<>();
    }

    /**
     * Adds a single shape to this box.
     */
    public void addShape(Shape shape) {

        storage.add(shape);
    }

    /**
     * Adds all the shapes in the given list to this box.
     * <p>
     * TODO modify the method signature so that things like Lists of Rectangles and Lists of Circles can be added, rather than just Lists of Shapes.
     *
     * @param shapes the shapes to add
     */
    public void addShapes(List<? extends Shape> shapes) {

        storage.addAll(shapes);
    }

    public double getTotalPerimeter() {

        // TODO Use a normal for-loop to add up the perimeter of all shapes in the list, and return the sum.
        // HINT: A single shape's perimeter can be obtained using the getPerimeter() method.

        double totalPerimeter = 0;

        for (int i = 0; i < storage.size(); i++) {

            totalPerimeter += storage.get(i).getPerimeter();
        }

        return totalPerimeter;
    }

    public double getTotalArea() {

        // TODO Use an enhanced for-loop to add up the area of all shapes in the list, and return the sum.
        // HINT: A single shape's area can be obtained using the getArea() method.

        int totalArea = 0;

        for (Shape shape : storage) {

            totalArea += shape.getArea();

        }

        return totalArea;
    }

    @Override
    public String toString() {

        // This formatter can be used to format numbers to 2DP when converting them to Strings
        NumberFormat twoDPFormat = new DecimalFormat("#.##");

        // TODO Set numShapes to the size of the storage list.
        int numShapes = 0;
        Iterator<Shape> myIterator = storage.iterator();

        String info = "ShapeBox [numShapes=" + numShapes +
                ", totalArea=" + twoDPFormat.format(getTotalArea()) +
                ", totalPerimeter=" + twoDPFormat.format(getTotalPerimeter()) +
                ", storage= [\n";

        // TODO Use an iterator to loop through all shapes in the list, and add their information to the info string.
        // HINT: Info about a shape can be obtained using its toString() method.

        while (myIterator.hasNext()) {

            String element = myIterator.next().toString();

            info = info + element + "\n";

        }

        info += "]]";

        return info;
    }
}
